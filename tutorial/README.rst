Ansible basics
==============

.. _`Ansible`: https://docs.ansible.com
.. _`apt`: http://docs.ansible.com/ansible/apt_module.html   
.. _`get_url`: http://docs.ansible.com/ansible/get_url_module.html   
.. _`register`: http://docs.ansible.com/ansible/playbooks_variables.html#registered-variables
.. _`shell`: http://docs.ansible.com/ansible/shell_module
.. _`unarchive`: http://docs.ansible.com/ansible/unarchive_module

This tutorial is all about practicing `Ansible`_. It covers 

* creation of an inventory,
* working with ``ansible`` command to run tasks from command line,
* creation of a playbook,
* working with ``ansible-playbook`` command. 

.. _`Setup environment`: setup.rst
.. _`Working copy`: working_copy.rst

Before you begin please read

* how to `Setup environment`_ for the class
* how to obtain a `Working copy`_ of the project for the class

Environment
-----------

To try out `Ansible`_ an environment with several hosts is needed. The
environment for the class comprises of three VMs based on `vm-2017-02-28`_ 
box.

+-----------+-------------------+---------------+----------------+
| Host      | eth0 (management) | eth1 (public) | eth2 (private) |
+-----------+-------------------+---------------+----------------+
| server101 | dhcp              | 172.16.0.101  | down           |
+-----------+-------------------+---------------+----------------+
| server102 | dhcp              | 172.16.0.103  | down           |
+-----------+-------------------+---------------+----------------+
| server103 | dhcp              | 172.16.0.103  | down           |
+-----------+-------------------+---------------+----------------+

Please refer to `Setup environment`_ for general information about environment.

Tasks
-----

.. _`Apache httpd 2.4 sources`: http://www-eu.apache.org/dist//httpd/httpd-2.4.25.tar.bz2
.. _`checksum`: https://www.apache.org/dist/httpd/httpd-2.4.25.tar.bz2.sha1   

#. Firstly, the *vagrant-public* network must be created and started. Run
   following commands: ::

      export VIRSH_DEFAULT_CONNECT_URI=qemu:///system 
      virsh net-define libvirt-network.xml
      virsh net-start vagrant-public
      virsh net-autostart vagrant-public

#. Start two VMs (leave the third for later): ::

     vagrant up --provider=libvirt server101 server102

#. Check if VMs are running using ``visrh`` command. You might use
   ``virt-manager`` alternatively. [1 point] ::

     operator@classroom: ~/02-ansible/tutorial $ virsh --connect qemu:///system list 
     
      Id    Name                    State
     ----------------------------------------------------
      48  tutorial_server102        running
      49  tutorial_server101        running

#. Create an inventory file ``hosts`` and define all VMs as hosts. Use
   ``ansible_host`` to set IP address ::

     server101    ansible_host=172.16.0.101
     server102    ansible_host=172.16.0.102
     server103    ansible_host=172.16.0.103

#. Test inventory using `ping`_ module, correct mistakes. [1 point]::

     ansible all -m ping

#. Open |deploy.yml|_ playbook. It is a skeleton that you will use for
   completing the rest of the tasks. 
#. Correct mistakes and run the playbook. ::

   ansible-playbook deploy.yml

#. Run it again. Observe differences.
#. Add a task to download `Apache httpd 2.4 sources`_ to ``/tmp``, use
   `get_url`_ module.
#. Add a task to unpack sources to ``/tmp``. Use `unarchive`_ module.
#. Add a task to configure and compile source code and install it to
   ``/usr/local`` (``--prefix`` option to ``configure``). Use `shell`_ module.
#. Add a task to start ``httpd``. Use `shell`_ module. [1 point]
#. Check if ``httpd`` is running on both active VMs.

   Create another play in |deploy.yml|_ playbook. Use shell_ module to check if
   process is running and `get_url`_ module to validate that web server is
   working as expected. Configure `get_url`_ to not use proxy server this time!

#. Start the third VM. Run the playbook again.
#. Check is ``httpd`` is running on all VMs.   
#. Check `checksum`_ of downloaded file, use `shell`_ module download file
   with checksum, parse it and save it as variable with `register`_.  [1 point]
#. Make playbook idempotent [2 points], use ``creates`` argument to `shell`_
   and `unarchive`_ module.

.. vim: spelllang=en spell textwidth=80 fo-=l: 
.. reformat paragraphs to 80 characters: select using v then hit gq
